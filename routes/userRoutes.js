const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController')
const auth = require('../auth')

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.logniUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	console.log(req.headers.authorization);

	userController.userProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
})


router.post("/enroll", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id,
		isAdmin: userData.isAdmin,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController =>res.send(resultFromController))
})


module.exports = router;	